  /*
   * This is the "definition section" which can contain any C code that should be copied to the
   * final program. Useful for #include declarations.
   */

%{
/*
 * this sample demonstrates (very) simple recognition:
 * a verb/not a verb
 */
%}

%%

  /*
   * This is the "rules section". Each rule consists of a pattern and an action, separated by
   * whitespace. The patterns are UNIX-style regexes and the actions are blocks of C code.
   *
   * The | (or) operator can be used to match multiple patterns with the same action.
   *
   * Lex disambiguates multiple matches by:
   * - matching a given input only once
   * - choosing the longest possible match for the current input
   */

[\t ]+      /* ignore whitespace */;

is |
am |
are |
were |
was |
be |
being |
been |
do |
does |
did  |
will |
would |
should |
can  |
could |
has  |
have |
had |
go          { printf("%s: is a verb\n", yytext); }

[a-zA-Z]+   { printf("%s: is not a verb\n", yytext); }

.|\n        { ECHO; }

%%

  /*
   * This is the "user subroutines section" which can contain any C code. It'll be copied to the
   * C file after the generated code.
  */

main()
{
  // This will be called automatically, even without this code.
  yylex();
}
