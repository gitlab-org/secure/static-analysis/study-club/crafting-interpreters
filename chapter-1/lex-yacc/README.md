# `lex` and `yacc`

Example lex specifications and programs reproduced from [lex & yacc, 2nd
Edition](https://www.oreilly.com/library/view/lex-yacc/9781565920002/).

Prerequisites:

```shell
brew install flex bison
```

Build and run:

``` shell
# make <name of directory>

make simple
./simple/simple.out
make clean
```

## ./simple

`simple.l` defines a lex specification that will echo the provided input. The
line

``` lex
.|\n    ECHO;
```

is a regex that matches all characters up to a newline, and calls the [`ECHO`
macro](https://westes.github.io/flex/manual/Actions.html#index-ECHO) for each
match.

## ./words
 
`words.l` is a more complex example demonstrating the three sections of a lex
specification (definitions, rules, and user subroutines), and multiple rules and
patterns.

