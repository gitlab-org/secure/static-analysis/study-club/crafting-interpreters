# Crafting Interpreters

![Book Cover](./img/ci_cover.jpg)

Welcome to the bookclub for [Crafting
Interpreters](https://www.craftinginterpreters.com)!

> Crafting Interpreters contains everything you need to implement a
> full-featured, efficient scripting language. You’ll learn both high-level
> concepts around parsing and semantics and gritty details like bytecode
> representation and garbage collection. Your brain will light up with new
> ideas, and your hands will get dirty and calloused. It’s a blast.
> 
> Starting from main(), you build a language that features rich syntax, dynamic
> typing, garbage collection, lexical scope, first-class functions, closures,
> classes, and inheritance. All packed into a few thousand lines of clean, fast
> code that you thoroughly understand because you write each one yourself.

## Process

- The book is available for [free online](https://www.craftinginterpreters.com),
  as a PDF, or a physical book. I highly recommend the physical book as the
  typography and layout are just :ok_hand:
- We'll aim to read and complete the exercises for a single chapter each week
  and discuss what we learnt in an issue.
- The book has us write two interpreters, one in Java and another in C. It might
  be a good idea to translate the code examples into Go as an additional
  challenge.

NB: [Discoto](https://gitlab.com/gitlab-org/secure/pocs/discussion-automation)
was being used in [previous
bookclubs](https://gitlab.com/gitlab-org/secure/static-analysis/study-club/writing-an-interpreter-in-go/-/issues/1)
to automatically organise topics and discussion points. Let's give it a try here
and see how it goes.
